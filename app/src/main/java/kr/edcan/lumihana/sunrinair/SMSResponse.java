package kr.edcan.lumihana.sunrinair;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;

/**
 * Created by kimok_000 on 2016-06-21.
 */
public class SMSResponse extends Service {
    private ResponeThread thread;
    private BluetoothSPP bluetoothSPP;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!bluetoothSPP.isBluetoothEnabled()) {
            bluetoothSPP.enable();
        } else {
            if (!bluetoothSPP.isServiceAvailable()) {
                bluetoothSPP.setupService();
                bluetoothSPP.startService(BluetoothState.DEVICE_OTHER);
                bluetoothSPP.autoConnect("?");
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }
    
    @Override
    public void onCreate() {
        super.onCreate();

        initBluetooth();
        
        thread = new ResponeThread(getApplicationContext());
        thread.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        thread = null;
    }
    
    private void initBluetooth(){
        bluetoothSPP = new BluetoothSPP(SMSResponse.this);
        if(!bluetoothSPP.isBluetoothAvailable())

        {
            Toast.makeText(getApplicationContext()
                    , "블루투스를 켜주세요"
                    , Toast.LENGTH_SHORT).show();
        }

        bluetoothSPP.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener()

        {
            public void onDeviceConnected(String name, String address) {
                Toast.makeText(getApplicationContext()
                        , "연결되었습니다", Toast.LENGTH_SHORT).show();
            }

            public void onDeviceDisconnected() {
                Toast.makeText(getApplicationContext()
                        , "연결이끊겼습니다"
                        , Toast.LENGTH_SHORT).show();
            }

            public void onDeviceConnectionFailed() {
            }
        });

        bluetoothSPP.setAutoConnectionListener(new BluetoothSPP.AutoConnectionListener() {
            public void onNewConnection(String name, String address) {
            }

            public void onAutoConnectionStarted() {
            }
        });
        bluetoothSPP.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            public void onDataReceived(byte[] data, String message) {
                if(!message.equals("")) {
                    String[] datas = message.split(",");
                    int temp = Integer.parseInt(datas[0]);
                    int humi = Integer.parseInt(datas[1]);
                    int isWater = Integer.parseInt(datas[2]);
                    Log.e("responseData", "temp : " + temp + ", humi : " + humi + ",isWater" + isWater);

                }
            }
        });
    }
}
