package kr.edcan.lumihana.sunrinair;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by kimok_000 on 2016-06-21.
 */
public class SMSRequest extends BroadcastReceiver {
    private static final String INTENT_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private String reqPhone;
    private String reqMessage;
    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(INTENT_SMS_RECEIVED)){
            this.context = context;
            Bundle bundle = intent.getExtras();

            if(bundle == null) return;

            Object[] pdusObj = (Object[]) bundle.get("pdus");
            if(pdusObj == null) return;

            SmsMessage[] smsMessages = new SmsMessage[pdusObj.length];
            for(int i = 0; i < pdusObj.length; i++){
                smsMessages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
            }

            reqMessage = smsMessages[0].getMessageBody().toString().trim();
            reqPhone = smsMessages[0].getOriginatingAddress().toString().trim();

            SMSList.add(new SMSData(reqPhone, reqMessage));
        }
    }
}
