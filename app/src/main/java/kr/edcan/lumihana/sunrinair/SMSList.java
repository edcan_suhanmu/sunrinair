package kr.edcan.lumihana.sunrinair;

import java.util.ArrayList;

/**
 * Created by kimok_000 on 2016-06-21.
 */
public class SMSList {
    private static int position=-1;
    private static ArrayList<SMSData> arrayList = new ArrayList<>();

    public static int add(SMSData data){
        arrayList.add(data);
        return position++;
    }

    public static void remove(int pos){
        arrayList.remove(pos);
        position--;
    }

    public static SMSData get(int position){
        return arrayList.get(position);
    }

    public static int getPosition(){
        return position;
    }
}
